package com.example.BostonCrimesMap

import org.apache.spark.sql.functions._

object Main extends App with SparkSessionWrapper {

  args.foreach(println)

  if (args.size < 3) {
    println("Please provide 3 arguments: path to crime.csv, path to offense_codes.csv, output path")
    System.exit(1)
  }

  val path_to_crime = args(0)// or "crime.csv"
  val path_to_offense_codes = args(1)// or "offense_codes.csv"
  val path_to_output_folder = args(2)


  val pad = udf { (s: String) => f"${s.toInt}%05d" }

  val getMedian = udf { (a: Seq[java.math.BigDecimal]) => a.sorted.apply((a.size - 1) * 0.5.toInt).toBigInteger } //sorted.apply(0)}//(((a.size-1)*0.5).toInt)}

  val joinFirstThreeItems = udf { (a: Seq[String]) => a.slice(0, 3).mkString(", ") }

  val offense_types_df = spark.read.format("csv").option("header", "true").load(path = path_to_offense_codes)
    .withColumn("OFFENSE_TYPE", split(col("NAME"), " - ").getItem(0))
    .withColumn("PADDED_CODE", pad(col("CODE")))

  val df = spark.read.format("csv").option("header", "true").load("crime.csv")
    .where(col("DISTRICT").isNotNull)
    .join(
    broadcast(offense_types_df),
    offense_types_df("PADDED_CODE") <=> col("OFFENSE_CODE"))
    .select("INCIDENT_NUMBER",
      "Lat",
      "Long",
      "MONTH",
      "YEAR",
      "DISTRICT",
      "OFFENSE_TYPE")

  val crimes_monthly = df.groupBy("DISTRICT", "MONTH", "YEAR").
    agg(count("INCIDENT_NUMBER").alias("mthcount"))
    .groupBy("DISTRICT")
    .agg(collect_list("mthcount").alias("MonthValues"))
    .withColumn("crimes_monthly", getMedian(col("MonthValues")))
    .drop("MonthValues") //.show()


  val frequent_crime_types = df.groupBy("DISTRICT", "OFFENSE_TYPE")
    .agg(count("INCIDENT_NUMBER").alias("crime_types_count"))
    .orderBy(asc("DISTRICT"), desc("crime_types_count"))
    .groupBy("DISTRICT")
    .agg(collect_list("OFFENSE_TYPE").alias("crime_types_list"))
    .withColumn("frequent_crime_types", joinFirstThreeItems(col("crime_types_list")))
    .drop("crime_types_list")

  val data = df.groupBy("DISTRICT").
    agg(count("INCIDENT_NUMBER").alias("crimes_total"),
      avg("Lat").alias("Lat"),
      avg("Long").alias("Lng"))

  data.join(crimes_monthly, Seq("DISTRICT")).
    join(frequent_crime_types, Seq("DISTRICT")).
    select("DISTRICT",
      "crimes_total",
      "crimes_monthly",
      "frequent_crime_types",
      "lat",
      "lng")
    .orderBy("DISTRICT").show()

  data.write.parquet(path_to_output_folder)
}



